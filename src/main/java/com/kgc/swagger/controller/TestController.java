package com.kgc.swagger.controller;

import com.kgc.swagger.User;
import io.swagger.annotations.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.websocket.server.PathParam;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
public class TestController {

    @ApiOperation(value = "Hello Spring Boot", notes = "没有参数Hello Spring Boot")
    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public String index() {
        System.out.println("hello spring boot");
        return "Hello Spring Boot";
    }

        @ApiOperation(value = "根据ID返回用户对象", notes = "根据ID返回用户对象RESTFUL风格")

        @RequestMapping(value = "/find/{id}", method = RequestMethod.GET)
        @ApiImplicitParam(name="id",value="用户编号",required = true,dataType = "int",paramType = "path")
        public User getById(@PathVariable int id) {
            System.out.println("id="+id);
            User user=new User();
            user.setId(id);
            return user;

}

    @ApiOperation(value = "用户登录", notes = "输入用户名与密码name与pwd")
    @ApiImplicitParams({@ApiImplicitParam(name="name",value="用户名",required = true,dataType = "String",paramType = "query"),@ApiImplicitParam(name="pwd",value="用户密码",required = true,dataType = "String",paramType = "query")
    })
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public User login(String name,String pwd) {
        System.out.println("name:"+name+","+"pwd:"+pwd);
        User user=new User();
        user.setName(name);
        user.setPwd(pwd);
        return user;

    }

    @ApiOperation(value = "用户登录", notes = "使用User对象封装提交的数据,注意用Post")
    @ApiImplicitParam(name="user",value="用户对象",required = true,dataType = "User",paramType = "body")
    @RequestMapping(value = "/loginUser", method = RequestMethod.POST)
    public User loginUser(@ApiParam @RequestBody(required=false) User user) {
        System.out.println("name:"+user.getName()+","+"pwd:"+user.getPwd());
        return user;
    }
    @ApiOperation(value = "用户登录", notes = "使用User对象封装查询条件,注意用Post")
    @ApiImplicitParam(name="user",value="用户对象",required = true,dataType = "User",paramType = "body")
    @RequestMapping(value = "/findAll", method = RequestMethod.POST)
    public List<User> findAll(@ApiParam @RequestBody(required=false) User user) {
        System.out.println("name:"+user.getName()+","+"pwd:"+user.getPwd());
        List<User> list=new ArrayList<>();
        list.add(new User(1,"admin","admin",23));
        list.add(new User(2,"hello","admin",23));
        list.add(new User(3,"java","admin",23));
        list.add(new User(4,"tom","admin",23));
        list.add(new User(5,"abc","admin",23));
        return list;
    }



}
